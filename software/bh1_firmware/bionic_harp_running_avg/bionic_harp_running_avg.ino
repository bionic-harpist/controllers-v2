/******************************
 * The Bionic Harp
 * Modular BLE MIDI controllers 
 * augmenting concrt harp
 * 
 * for the Wemos Lolin D32 PRO

 * 
 * https://gitlab.com/bionic-harpist/controllers
 * John D. Sullivan 
 * 
 ******************************/

#include "Adafruit_NeoTrellis.h"
#include <BLEMidi.h>

#define MODULE 0  // 0 == LEFT
                  // 1 == RIGHT

#define DELTIME 1
bool DEBUG = false; // print output to serial

#define NUM_ANALOG_INPUTS 8
#define NUM_TRELLIS_INPUTS 16
#define MV_AVG_BUFFER_SIZE 75

// Pin definitions

// input pins for analog knobs and sliders
const uint8_t analogPins[NUM_ANALOG_INPUTS] = {25, 26, 27, 15, 33, 32, 14, 4};
#define LEDPIN 5

uint8_t thisVal = 0;

long prevMillis = 0;
long currMillis = 0;
bool ledState = false;

// MIDI values to send
uint8_t valsAn[NUM_ANALOG_INPUTS] = {0,0,0,0,0,0,0,0}; // analog inputs
bool analogReverse[NUM_ANALOG_INPUTS] = {true, true, true, true, false, false, false, false};
uint8_t valsTr[NUM_TRELLIS_INPUTS] = {   // trellis inputs
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

// last input values
uint8_t lastValsAn[NUM_ANALOG_INPUTS] = {0,0,0,0,0,0,0,0}; // last analog inputs
uint8_t lastValsTr[NUM_TRELLIS_INPUTS] = {   // trellis inputs
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

// MIDI CC#s
uint8_t midiCcAn[2][NUM_ANALOG_INPUTS] = { // analog inputs, LH and RH
  // CC# 20 - 23, 40-43 LH
  {0x14,0x15,0x16,0x17,0x28,0x29,0x2a,0x2b},
  // CC# 44 - 51 RH
  {0x2c,0x2d,0x2e,0x2f,0x30,0x31,0x32,0x33}};
uint8_t midiCcTr[2][NUM_TRELLIS_INPUTS] = {   // trellis inputs, LH and RH
  // CC# 24 - 39 (TRELLIS) LH
  {0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,
  0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27},
  // CC 52 - 67 (TRELLIS) RH
  {0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,
  0x3c,0x3d,0x3e,0x3f,0x40,0x41,0x42,0x43}};

// input type (0 = momentary button, 1 = on/off, 2 = continuous)
uint8_t inTypeAn[NUM_ANALOG_INPUTS] = {2,2,2,2,2,2,2,2}; // analog inputs
uint8_t inTypeTr[NUM_TRELLIS_INPUTS] = {   // trellis inputs
  0,0,1,1,0,0,0,0,1,1,1,1,0,0,0,0};

uint16_t readings[NUM_ANALOG_INPUTS][MV_AVG_BUFFER_SIZE];
int totals[NUM_ANALOG_INPUTS];

Adafruit_NeoTrellis trellis; // create a Trellis object

void setup() {
  Serial.begin(115200);
  
  if(MODULE == 0) {
    BLEMidiServer.begin("Bionic_Harp_LH");
  } else {
    BLEMidiServer.begin("Bionic_Harp_RH");
  }
  BLEMidiServer.enableDebugging();

  pinMode(LEDPIN, OUTPUT);

  // input pins for the analog inputs
  for(uint8_t i=0;i<NUM_ANALOG_INPUTS;i++) { 
    pinMode(analogPins[i], INPUT);
  }
  
  if (!trellis.begin()) {
    Serial.println("Could not start trellis, check wiring?");
    while(1);
  } else {
    Serial.println("NeoPixel Trellis started");
  }
  
  //activate all keys and set callbacks
  for(uint8_t i=0; i<NEO_TRELLIS_NUM_KEYS; i++){
    trellis.activateKey(i, SEESAW_KEYPAD_EDGE_RISING);
    trellis.activateKey(i, SEESAW_KEYPAD_EDGE_FALLING);
    trellis.registerCallback(i, trellisInput);
  }
  
  //do a little animation to show we're on
  for (uint16_t i=0; i<trellis.pixels.numPixels(); i++) {
    trellis.pixels.setPixelColor(i, Wheel(map(i, 0, trellis.pixels.numPixels(), 0, 255)));
    trellis.pixels.show();
    delay(50);
  }
  for (uint16_t i=0; i<trellis.pixels.numPixels(); i++) {
    trellis.pixels.setPixelColor(i, 0x000000);
    trellis.pixels.show();
    delay(50);
  }

  // initialize variables for pot smoothing  
  for(uint8_t i=0;i<NUM_ANALOG_INPUTS;i++) {
    totals[i] = 0;
    for(uint8_t j=0;j<MV_AVG_BUFFER_SIZE;j++) {
      readings[i][j] = 0;
    }
  }
  
  Serial.println("The Bionic Harp"); 
  if (MODULE == 0) {
    Serial.println("LEFT module");
  } else {
    Serial.println("RIGHT module");
  }
  for(uint8_t i=0; i<10; i++){
    digitalWrite(LEDPIN, HIGH);
    Serial.print(". ");
    delay(100);
    digitalWrite(LEDPIN, LOW);
    delay(100);
  }
  Serial.println("");
}

void loop() {
  
  // just blink the light to know we're alive...
  /*currMillis = millis();
  if ( currMillis > prevMillis + 667) {
    if (ledState) {
      digitalWrite(LEDPIN, LOW);
      ledState = false;
    } else {
      digitalWrite(LEDPIN, HIGH);
      ledState = true;
    }
    prevMillis = currMillis;
  } */

  readAnalogValues();
  readTrellis();

  delay(DELTIME);
}

void readAnalogValues() {
  for(uint8_t i=0; i<NUM_ANALOG_INPUTS; i++) {
    thisVal = translate(analogRead(analogPins[i]), inTypeAn[i], i); 
    if(thisVal != lastValsAn[i]) {
      switch(inTypeAn[i]) {
        case 0: // new momentary button state (0 or 127)
          valsAn[i] = thisVal;
          break;
        case 1: // toggle - only fire if button val is 127
          if(thisVal == 127) {
            if(valsAn[i] == 0) {
              valsAn[i] = 127;
            } else {
              valsAn[i] = 0;
            }
          }
          break;
        case 2: // continuous control (0 to 127)
          valsAn[i] = thisVal;
          break;       
      }
      if(inTypeAn[i]==1 && thisVal!=127) {
        // do nothing 
      } else {
        sendMIDImessage(midiCcAn[MODULE][i], valsAn[i]);
        if(DEBUG) {
          Serial.print(i);  
          Serial.print(": "); 
          Serial.println(valsAn[i]);   
        }
      }
      lastValsAn[i] = thisVal; 
    }   
  }
}

void readTrellis() {
  trellis.read(); // get data from the trellis
  for(uint8_t z=0; z<16; z++) { // cycle through all the buttons
    if(valsTr[z] != lastValsTr[z]) {
      sendMIDImessage(midiCcTr[MODULE][z], valsTr[z]);
      lastValsTr[z] = valsTr[z];

      if(DEBUG) {
        Serial.print("TREL-"); 
        Serial.print(z); 
        Serial.print(": "); 
        Serial.println(valsTr[z]);
      }
    }
  }
}

uint8_t translate(uint16_t val, uint8_t type, uint8_t idx) {
  // inputs: 
  //    val:      input value (0 - 4095)
  //    type:     0 (momentary) / 1 (toggle) / 2 (continuous)
  //    idx        0 - NUM_ANALOG_INPUTS which analog input
  //    lastVal:  last output val (check for change for type 1)

  uint8_t translatedValue;
  
  switch(type) {
    case 0: 
      if(val >=2047) {
        translatedValue = 127;
      } else {
        translatedValue = 0;
      }
      return translatedValue; 
    case 1: 
      if(val >=2047) {
        translatedValue = 127;
      } else {
        translatedValue = 0;
      }
      return translatedValue;
    case 2: 
      // take average of last n readings

      // running total: subtract oldest (n-1) and add incoming
      totals[idx] = totals[idx] - readings[idx][MV_AVG_BUFFER_SIZE-1] + val;
      
      uint16_t average = totals[idx]/MV_AVG_BUFFER_SIZE; // get the average of last n readings

      for(uint8_t i=MV_AVG_BUFFER_SIZE-1;i>0;i--) { // count down numReadings-1 to 0;
        readings[idx][i] = readings[idx][i-1]; // move readings over a place
      }
      readings[idx][0] = val; // add incoming val to 1st position in array

      // translate value to MIDI 0-127
      translatedValue = uint8_t(round(map(average, 0, 4095, 0, 127)));
      if (analogReverse[idx]) {
        translatedValue = 127 - translatedValue;
      }
//      Serial.println(translatedValue);
      return translatedValue;
  }
}

void sendMIDImessage (uint8_t cc, uint8_t val) {
  if(BLEMidiServer.isConnected()) {
    BLEMidiServer.controlChange(0, cc, val);

    if(DEBUG) {
      Serial.print("Sending MIDI: "); 
      Serial.print(cc);
      Serial.print(": ");
      Serial.print(val); 
      Serial.println(); 
    }
  } else {
    Serial.println("BLE not connected!!");
  }
}
