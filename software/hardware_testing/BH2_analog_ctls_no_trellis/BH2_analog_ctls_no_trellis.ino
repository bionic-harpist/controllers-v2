#define SLIDER1 33
#define SLIDER2 32
#define SLIDER3 14
#define SLIDER4 4
#define KNOB1 25
#define KNOB2 26
#define KNOB3 27
#define KNOB4 15
#define LED1 5
#define LED2 18
#define LED3 23
#define LED4 19

int lastMillis = 0;
int thresh = 125;
int ledCount = 0;


void setup() {

  Serial.begin(9600);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(LED4, OUTPUT);
  
}


void loop() {

  // Read analog inputs
  int k1 = map(analogRead(KNOB1), 0, 4092, 0, 9);
  int k2 = map(analogRead(KNOB2), 0, 4092, 0, 9);
  int k3 = map(analogRead(KNOB3), 0, 4092, 0, 9);
  int k4 = map(analogRead(KNOB4), 0, 4092, 0, 9);
  int s1 = map(analogRead(SLIDER1), 0, 4092, 0, 9);
  int s2 = map(analogRead(SLIDER2), 0, 4092, 0, 9);
  int s3 = map(analogRead(SLIDER3), 0, 4092, 0, 9);
  int s4 = map(analogRead(SLIDER4), 0, 4092, 0, 9);
  
  // Print analog inputs to serial monitor
  Serial.print(k1);
  Serial.print("\t");
  Serial.print(k2);
  Serial.print("\t");
  Serial.print(k3);
  Serial.print("\t");
  Serial.print(k4);
  Serial.print("\t");
  Serial.print(s1);
  Serial.print("\t");
  Serial.print(s2);
  Serial.print("\t");
  Serial.print(s3);
  Serial.print("\t");
  Serial.println(s4);

  // blink the leds
  switch(ledCount%8) {
    case 0:
      digitalWrite(LED1, HIGH);
      digitalWrite(LED2, LOW);
      digitalWrite(LED3, LOW);
      digitalWrite(LED4, LOW);
      break;
    case 1:
      digitalWrite(LED1, HIGH);
      digitalWrite(LED2, HIGH);
      digitalWrite(LED3, LOW);
      digitalWrite(LED4, LOW);
      break;
    case 2:
      digitalWrite(LED1, HIGH);
      digitalWrite(LED2, HIGH);
      digitalWrite(LED3, HIGH);
      digitalWrite(LED4, LOW);
      break;
    case 3:
      digitalWrite(LED1, HIGH);
      digitalWrite(LED2, HIGH);
      digitalWrite(LED3, HIGH);
      digitalWrite(LED4, HIGH);
      break;
    case 4:
      digitalWrite(LED1, LOW);
      digitalWrite(LED2, HIGH);
      digitalWrite(LED3, HIGH);
      digitalWrite(LED4, HIGH);
      break;
    case 5:
      digitalWrite(LED1, LOW);
      digitalWrite(LED2, LOW);
      digitalWrite(LED3, HIGH);
      digitalWrite(LED4, HIGH);
      break;
    case 6:
      digitalWrite(LED1, LOW);
      digitalWrite(LED2, LOW);
      digitalWrite(LED3, LOW);
      digitalWrite(LED4, HIGH);
      break;
    case 7:
      digitalWrite(LED1, LOW);
      digitalWrite(LED2, LOW);
      digitalWrite(LED3, LOW);
      digitalWrite(LED4, LOW);
      break;
    default:
      digitalWrite(LED1, LOW);
      digitalWrite(LED2, LOW);
      digitalWrite(LED3, LOW);
      digitalWrite(LED4, LOW);
      break;
  }
  ledCount ++;
  
  delay(thresh);
}